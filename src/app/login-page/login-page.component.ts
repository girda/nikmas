import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  form: FormGroup;
  aSub: Subscription;
  constructor(private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required, Validators.email]),
      pass: new FormControl(null, [Validators.required, Validators.minLength(6)]),
    });

    // this.route.queryParams.subscribe((params: Params) => {
    //   if (params['registered']) {
    //     // login success
    //   } else if (params['accessDenied']) {
    //     // login error
    //   }
    // });
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  onSubmit() {
    this.form.disable();

    const user = {
      login: this.form.value.login,
      pass: this.form.value.pass
    };

    this.aSub = this.auth.login(user).subscribe(
      () => {
        console.log('login success');
      },
      error => {
        console.log(error);
        this.router.navigate(['/finance']).then(r => {
          console.log(r);
        });
        this.form.enable();
      }
    );
  }

}
