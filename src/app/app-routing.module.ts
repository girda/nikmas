import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import {AuthLayoutComponent} from './shared/layouts/auth-layout/auth-layout.component';
import {MainLayoutComponent} from './shared/layouts/main-layout/main-layout.component';
import {RegisterPageComponent} from './pages/register-page/register-page.component';
import {CompaniesPageComponent} from './pages/companies-page/companies-page.component';
import {SettingsPageComponent} from './pages/settings-page/settings-page.component';
import {HelpPageComponent} from './help-page/help-page.component';
import {CompanyAssetsComponent} from './pages/companies-page/company-assets/company-assets.component';
import {CompanyStaffComponent} from './pages/companies-page/company-staff/company-staff.component';
import {CompanyFinanceComponent} from './pages/companies-page/company-finance/company-finance.component';


const routes: Routes = [
  {
    path: '', component: AuthLayoutComponent, children: [
      {path: '', redirectTo: '/login', pathMatch: 'full'},
      {path: 'login', component: LoginPageComponent},
      {path: 'register', component: RegisterPageComponent}
    ]
  },
  {
    path: '', component: MainLayoutComponent, children: [
      {path: 'companies', component: CompaniesPageComponent},
      {path: 'settings', component: SettingsPageComponent},
      {path: 'help', component: HelpPageComponent},
      {path: 'assets', component: CompanyAssetsComponent},
      {path: 'staff', component: CompanyStaffComponent},
      {path: 'finance', component: CompanyFinanceComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
