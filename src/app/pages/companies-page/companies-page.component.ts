import {Component, OnInit, ViewChildren} from '@angular/core';
import {CompaniesService} from '../../shared/services/companies.service';
import {AmChartsService} from '../../shared/services/am-charts.service';

@Component({
  selector: 'app-companies-page',
  templateUrl: './companies-page.component.html',
  styleUrls: ['./companies-page.component.scss']
})
export class CompaniesPageComponent implements OnInit {
  // indicators: any[] = [
  //   {
  //     title: 'Финансы',
  //     id: 'finance',
  //     children: [
  //       {id: 'debit', title: 'На рахунках', checked: true},
  //       {id: 'credit', title: 'Ми винні', checked: false},
  //       {id: 30, title: 'Нам винні', checked: false}
  //     ]
  //   },
  //   {
  //     title: 'Персонал на',
  //     id: 'staff',
  //     children: [
  //       {id: 40, title: 'Кількість, осіб', checked: false},
  //       {id: 50, title: 'Фонд з/п', checked: false},
  //       {id: 60, title: 'Фонд з/п + нарахування', checked: false}
  //     ]
  //   },
  //   {
  //     title: 'Активы на',
  //     id: 'assets',
  //     children: [
  //       {id: 70, title: 'Основні засоби', checked: false},
  //       {id: 80, title: 'Нематеріальні активи', checked: false},
  //       {id: 90, title: 'Залишки на складах', checked: false}
  //     ]
  //   }
  // ];

  // @ViewChildren('indicator', null) indicatorElements;
  //
  // constructor(private companiesService: CompaniesService,
  //             private amCharts: AmChartsService) {
  // }
  //
  ngOnInit() {
    // this.companiesService.updateCompaniesIndicators(this.indicators);
  }
  //
  // onChangeIndicators(event) {
  //   this.indicatorElements._results.forEach(input => {
  //     this.indicators.forEach(indicator => {
  //       indicator.children.forEach(child => {
  //         if (child.id === input.nativeElement.id) {
  //           child.checked = input.nativeElement.checked;
  //         }
  //       });
  //     });
  //   });
  //   this.companiesService.updateCompaniesIndicators(this.indicators);
  //   let exitFlag = false;
  //
  //   for (let i = 0; i < this.indicators.length; i++) {
  //     for (let j = 0; j < this.indicators[i].children.length; j ++) {
  //       if (this.indicators[i].children[j].checked) {
  //         this.amCharts.render();
  //         exitFlag = true;
  //         break;
  //       } else if (i === this.indicators.length - 1 && j === this.indicators[i].children.length - 1) {
  //         this.amCharts.destroy();
  //       }
  //     }
  //     if (exitFlag) {
  //       break;
  //     }
  //   }
  // }
  //
  // updateIndicators(companies) {
  //   console.log(companies);
  //   let debit = 0;
  //   let credit = 0;
  //   companies.forEach(company => {
  //     debit += company.finance.debit;
  //     credit += company.finance.credit;
  //   });
  //
  //   this.indicators.forEach(parent => {
  //     parent.children.forEach(indicator => {
  //       if (indicator.id === 'debit') {
  //         indicator.total = debit;
  //       }
  //       if (indicator.id === 'credit') {
  //         indicator.total = credit;
  //       }
  //     });
  //   });
  // }

}
