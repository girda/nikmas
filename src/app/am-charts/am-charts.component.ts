import {Component, OnInit, OnDestroy, AfterViewInit, NgZone} from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import {AmChartsService} from '../shared/services/am-charts.service';

am4core.useTheme(am4themes_animated);
@Component({
  selector: 'app-am-charts',
  templateUrl: './am-charts.component.html',
  styleUrls: ['./am-charts.component.scss']
})
export class AmChartsComponent implements OnInit {

  constructor(private zone: NgZone,
              private amCharts: AmChartsService) { }

  ngOnInit() {
  }

  // ngAfterViewInit() {
  //   this.zone.runOutsideAngular(() => {
  //     this.amCharts.render();
  //   });
  // }
  //
  // ngOnDestroy() {
  //   this.zone.runOutsideAngular(() => {
  //     this.amCharts.destroy();
  //   });
  // }
}
