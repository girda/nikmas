import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {CompaniesService} from './shared/services/companies.service';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import {AuthService} from './shared/services/auth.service';
import { CompaniesPageComponent } from './pages/companies-page/companies-page.component';
import { HelpPageComponent } from './help-page/help-page.component';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import { AmChartsComponent } from './am-charts/am-charts.component';
import {AmChartsService} from './shared/services/am-charts.service';
import { TestComponent } from './test/test.component';
import { CompanyStaffComponent } from './pages/companies-page/company-staff/company-staff.component';
import { CompanyFinanceComponent } from './pages/companies-page/company-finance/company-finance.component';
import { CompanyAssetsComponent } from './pages/companies-page/company-assets/company-assets.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AuthLayoutComponent,
    MainLayoutComponent,
    RegisterPageComponent,
    CompaniesPageComponent,
    HelpPageComponent,
    SettingsPageComponent,
    AmChartsComponent,
    TestComponent,
    CompanyStaffComponent,
    CompanyFinanceComponent,
    CompanyAssetsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService, CompaniesService, AmChartsService, CompaniesPageComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
