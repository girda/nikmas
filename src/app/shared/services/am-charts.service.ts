import {Injectable} from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import {CompaniesService} from './companies.service';
import {string} from '@amcharts/amcharts4/core';

am4core.useTheme(am4themes_animated);

@Injectable()
export class AmChartsService {
  private lines;
  private charts = [
    {
      finance: {title: 'На счетах', linkForCompany: 'debit'},
      staff: {title: 'Количество человек', linkForCompany: 'quantity_people'},
      assets: {title: 'Основные средства', linkForCompany: 'fixed_assets'},
      chart: null
    },
    {
      finance: {title: 'Мы должны', linkForCompany: 'credit'},
      staff: {title: 'Фонд з/п', linkForCompany: 'fund'},
      assets: {title: 'Нематериальные активы', linkForCompany: 'intangible_assets'},
      chart: null
    },
    {
      finance: {title: 'Нам должны', linkForCompany: 'should'},
      staff: {title: 'Фонд з / п + начисления', linkForCompany: 'fund_accrual'},
      assets: {title: 'Остатки на складах', linkForCompany: 'remains_warehouses'},
      chart: null
    }
  ];


  constructor(private companiesServices: CompaniesService) {
  }

  destroy() {
    this.charts.forEach(chart => {
      console.log(chart);
      if (chart.chart) {

        chart.chart.dispose();
        chart.chart = null;
        console.log(chart);
      }
    });
    if (this.lines) {
      this.lines.dispose();
    }
  }

  render() {
    const checkDispose = () => {
      return new Promise(resolve => {
        if (this.lines) {
          this.lines.dispose();
        }
        this.charts.forEach((chart, i) => {
          console.log(chart);
          if (chart.chart) {
            chart.chart.dispose();
          }
          if (i + 1 === this.charts.length) {
            resolve(true);
          }
        });
      });
    };

    checkDispose().then(ok => {

      let activeCompaniesFlag = false;
      const companies = this.companiesServices.getActiveCompanies();
      const indicator: any[string] = this.companiesServices.getCompaniesIndicators();
      const createObj = (title, link) => {
        const obj = {};
        obj[title] = {};
        companies.forEach(company => {
          console.log(company);
          if (company.active) {
            switch (link) {
              case 'debit':
                obj[title][`${company.title.slice(0, 9)}...`] = company.finance.debit;
                break;
              case 'credit':
                obj[title][`${company.title.slice(0, 9)}...`] = company.finance.credit;
                break;
              case 'should':
                obj[title][`${company.title.slice(0, 9)}...`] = company.finance.should;
                break;
              case 'quantity_people':
                obj[title][`${company.title.slice(0, 9)}...`] = company.staff.quantity_people;
                break;
              case 'fixed_assets':
                obj[title][`${company.title.slice(0, 9)}...`] = company.assets.fixed_assets;
                break;
              case 'fund':
                obj[title][`${company.title.slice(0, 9)}...`] = company.staff.fund;
                break;
              case 'intangible_assets':
                obj[title][`${company.title.slice(0, 9)}...`] = company.assets.intangible_assets;
                break;
              case 'fund_accrual':
                obj[title][`${company.title.slice(0, 9)}...`] = company.staff.fund_accrual;
                break;
              case 'remains_warehouses':
                obj[title][`${company.title.slice(0, 9)}...`] = company.assets.remains_warehouses;
                break;
            }
          }
        });
        return obj;
      };

      if (companies.length) {
        companies.forEach(company => {
          if (company.active) {
            activeCompaniesFlag = true;
          }
        });
      }

      if (companies && companies.length && activeCompaniesFlag) {
        switch (indicator) {
          case 'finance':
            this.charts.forEach((chart, i) => {
              this._renderColumnGraph(createObj(chart.finance.title, chart.finance.linkForCompany), `chart-${i + 1}`, chart);
            });
            break;
          case'staff':
            this.charts.forEach((chart, i) => {
              this._renderColumnGraph(createObj(chart.staff.title, chart.staff.linkForCompany), `chart-${i + 1}`, chart);
            });
            break;
          case'assets':
            this.charts.forEach((chart, i) => {
              this._renderColumnGraph(createObj(chart.assets.title, chart.assets.linkForCompany), `chart-${i + 1}`, chart);
            });
            break;
        }
        this._renderLines();
      }
    });
  }

  _renderColumnGraph(data, classElement, chart) {
    chart.chart = am4core.create(classElement, am4charts.XYChart);
    chart.chart.paddingBottom = 50;

    const colors = {};

    const categoryAxis = chart.chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = 'category';
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.dataItems.template.text = '{realName}';
    categoryAxis.adapter.add('tooltipText', (tooltipText, target) => {
      return categoryAxis.tooltipDataItem.dataContext.realName;
    });

    const valueAxis = chart.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.min = 0;

    // single column series for all data
    const columnSeries = chart.chart.series.push(new am4charts.ColumnSeries());
    columnSeries.columns.template.width = am4core.percent(20);
    columnSeries.tooltipText = '{realName}, {valueY}';
    columnSeries.dataFields.categoryX = 'category';
    columnSeries.dataFields.valueY = 'value';

    // second value axis for quantity
    const valueAxis2 = chart.chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis2.renderer.opposite = true;
    valueAxis2.syncWithAxis = valueAxis;
    valueAxis2.tooltip.disabled = true;

    // quantity line series
    const lineSeries = chart.chart.series.push(new am4charts.LineSeries());
    lineSeries.tooltipText = '{valueY}';
    lineSeries.dataFields.categoryX = 'category';
    lineSeries.dataFields.valueY = 'quantity';
    lineSeries.yAxis = valueAxis2;
    lineSeries.bullets.push(new am4charts.CircleBullet());
    lineSeries.stroke = chart.chart.colors.getIndex(13);
    lineSeries.fill = lineSeries.stroke;
    lineSeries.strokeWidth = 2;
    lineSeries.snapTooltip = true;

    // when data validated, adjust location of data item based on count
    lineSeries.events.on('datavalidated', function() {
      lineSeries.dataItems.each(function(dataItem) {
        // if count divides by two, location is 0 (on the grid)
        if (dataItem.dataContext.count / 2 == Math.round(dataItem.dataContext.count / 2)) {
          dataItem.setLocation('categoryX', 0);
        } else {
          dataItem.setLocation('categoryX', 0.5);
        }
      });
    });

    // fill adapter, here we save color value to colors object so that each time the item has the same name, the same color is used
    columnSeries.columns.template.adapter.add('fill', (fill, target) => {
      const name = target.dataItem.dataContext.realName;
      // console.log(name);
      console.log(colors[name]);
      if (!colors[name]) {
        colors[name] = chart.chart.colors.next();
      }
      target.stroke = colors[name];
      return colors[name];

    });

    // columnSeries.fill = am4core.color("green");
    // console.log(columnSeries);

    ///// DATA
    const chartData = [];

    console.log(data);
    // process data ant prepare it for the chart
    for (const providerName in data) {
      const providerData = data[providerName];

      // add data of one provider to temp array
      const tempArray = [];
      let count = 0;
      // add items
      for (const itemName in providerData) {
        if (itemName !== 'quantity') {
          count++;
          // we generate unique category for each column (providerName + "_" + itemName) and store realName
          tempArray.push({
            category: providerName + '_' + itemName,
            realName: itemName,
            value: providerData[itemName],
            provider: providerName
          });
        }
      }

      // add quantity and count to middle data item (line series uses it)
      const lineSeriesDataIndex = Math.floor(count / 2);
      tempArray[lineSeriesDataIndex].quantity = providerData.quantity;
      tempArray[lineSeriesDataIndex].count = count;
      // push to the final data
      am4core.array.each(tempArray, (item) => {
        chartData.push(item);
      });

      // create range (the additional label at the bottom)
      const range = categoryAxis.axisRanges.create();
      range.category = tempArray[0].category;
      range.endCategory = tempArray[tempArray.length - 1].category;
      range.label.text = tempArray[0].provider;
      range.label.dy = 30;
      range.label.truncate = true;
      range.label.fontWeight = 'bold';
      range.label.tooltipText = tempArray[0].provider;

      range.label.adapter.add('maxWidth', (maxWidth, target) => {
        const range = target.dataItem;
        const startPosition = categoryAxis.categoryToPosition(range.category, 0);
        const endPosition = categoryAxis.categoryToPosition(range.endCategory, 1);
        const startX = categoryAxis.positionToCoordinate(startPosition);
        const endX = categoryAxis.positionToCoordinate(endPosition);
        return endX - startX;
      });
    }

    chart.chart.data = chartData;


    // last tick
    const range = categoryAxis.axisRanges.create();
    range.category = chart.chart.data[chart.chart.data.length - 1].category;
    range.label.disabled = true;
    range.tick.location = 1;
    range.grid.location = 1;
  }

  _renderLines() {
    this.lines = am4core.create('chart-lines', am4charts.XYChart);
    this.lines.data = [
      {date: new Date(2019, 5, 12), value1: 50, value2: 48, value3: 55, previousDate: new Date(2019, 5, 5)},
      {date: new Date(2019, 5, 13), value1: 53, value2: 51, value3: 46, previousDate: new Date(2019, 5, 6)},
      {date: new Date(2019, 5, 14), value1: 56, value2: 58, value3: 45, previousDate: new Date(2019, 5, 7)},
      {date: new Date(2019, 5, 15), value1: 52, value2: 53, value3: 48, previousDate: new Date(2019, 5, 8)},
      {date: new Date(2019, 5, 16), value1: 48, value2: 44, value3: 49, previousDate: new Date(2019, 5, 9)},
      {date: new Date(2019, 5, 17), value1: 47, value2: 42, value3: 45, previousDate: new Date(2019, 5, 10)},
      {date: new Date(2019, 5, 18), value1: 59, value2: 55, value3: 43, previousDate: new Date(2019, 5, 11)}
    ];

    // Create axes
    const dateAxis = this.lines.xAxes.push(new am4charts.DateAxis());
    dateAxis.renderer.minGridDistance = 50;

    const valueAxis = this.lines.yAxes.push(new am4charts.ValueAxis());

// Create series
    const series = this.lines.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = 'value1';
    series.dataFields.dateX = 'date';
    series.strokeWidth = 2;
    series.minBulletDistance = 10;
    series.tooltipText = '[bold]{date.formatDate()}:[/] {value1}\n[bold]{previousDate.formatDate()}:[/] {value2}';
    series.tooltip.pointerOrientation = 'vertical';

// Create series
    const series2 = this.lines.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = 'value2';
    series2.dataFields.dateX = 'date';
    series2.strokeWidth = 2;
    series2.stroke = series.stroke;
    series2.stroke = am4core.color("red");

    // Create series
    const series3 = this.lines.series.push(new am4charts.LineSeries());
    series3.dataFields.valueY = 'value3';
    series3.dataFields.dateX = 'date';
    series3.strokeWidth = 2;
    series3.stroke = series.stroke;
    series3.stroke = am4core.color("green");

// Add cursor
    this.lines.cursor = new am4charts.XYCursor();
    this.lines.cursor.xAxis = dateAxis;
  }


}
