import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';

@Injectable()
export class CompaniesService {
  companies: any[] = [];
  companiesIndicators: any[];
  activeCompanies = [];

  constructor(private http: HttpClient) {}

  updateActiveCompanies(companies) {
    this.activeCompanies = companies;
  }

  updateCompaniesIndicators(indicators) {
    this.companiesIndicators = indicators;
  }

  getActiveCompanies() {
    return this.activeCompanies;
  }

  getCompanies() {
    return new Promise((resole, reject) => {
      if (this.companies.length) {
        resole(this.companies);
      } else {
        return this.http.get<[]>('/assets/companies.json').subscribe(
          res => {
            this.companies = res;
            resole(this.companies);
          },
          error => {
            reject(error);
          }
        );
      }
    });
  }

  getCompaniesIndicators() {
    return this.companiesIndicators;
  }
}


