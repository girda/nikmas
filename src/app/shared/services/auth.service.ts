import {Injectable} from '@angular/core';
import {User} from '../interfaces';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {}

  login(user: User): Observable<{}> {
    return this.http.post<{}>('url', user);
  }

  register() {}

}
