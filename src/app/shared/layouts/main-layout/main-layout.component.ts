import {Component, OnInit, ViewChildren} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CompaniesService} from '../../services/companies.service';
import {AmChartsService} from '../../services/am-charts.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {
  menu: any [] = [
    {
      link: 'companies',
      title: 'Компании',
      id: 'companies',
      active: true,
      children: []
    },
    {
      link: 'settings',
      title: 'Настройки',
      id: 'settings',
      active: false,
      children: [
        {title: 'child settings 1'},
        {title: 'child settings 2'}
      ]
    },
    {
      link: 'help',
      title: 'Помощь',
      id: 'help',
      active: false,
      children: [
        {title: 'child help 1'},
        {title: 'child help 2'}
      ]
    },
  ];
  locationBusiness: any[] = [
    {
      id: 'ua',
      title: 'UA',
      imgSrc: 'ua.png'
    },
    {
      id: 'ru',
      title: 'RU',
      imgSrc: 'ru.png'
    },
    {
      id: 'by',
      title: 'BY',
      imgSrc: 'by.png'
    }
  ];
  companies;
  activeCompanies: any[] = [];
  indicators: any[] = [
    {
      title: 'Финансы',
      id: 'finance',
      children: [
        {id: 'debit', title: 'На рахунках', checked: true},
        {id: 'credit', title: 'Ми винні', checked: false},
        {id: 'should', title: 'Нам винні', checked: false}
      ]
    },
    {
      title: 'Персонал',
      id: 'staff',
      children: [
        {id: 40, title: 'Кількість, осіб', checked: false},
        {id: 50, title: 'Фонд з/п', checked: false},
        {id: 60, title: 'Фонд з/п + нарахування', checked: false}
      ]
    },
    {
      title: 'Активы',
      id: 'assets',
      children: [
        {id: 70, title: 'Основні засоби', checked: false},
        {id: 80, title: 'Нематеріальні активи', checked: false},
        {id: 90, title: 'Залишки на складах', checked: false}
      ]
    }
  ];
  @ViewChildren('indicator', null) indicatorElements;
  constructor(private http: HttpClient,
              private companiesServices: CompaniesService,
              private amCharts: AmChartsService) {
  }

  ngOnInit() {
    this.companiesServices.updateCompaniesIndicators(location.pathname.substr(1)); // start position
    this.companiesServices.getCompanies().then(comp => {
      this.companies = comp;
      this.companies.forEach(company => {
        if (company.active) {
          this.activeCompanies.push(company);
        }
      });
      this.companiesServices.updateActiveCompanies(this.activeCompanies);
      this.amCharts.render();
    });
  }

  onChangeBusiness(event) {
    this.companiesServices.getCompanies().then(
      (companies: any[]) => {
        let flagRerender = false;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < companies.length; i++) {
          if (event.target.id === companies[i].parent && !event.target.checked) {
            for (let j = 0; j < this.activeCompanies.length; j++) {
              const activeCompany = this.activeCompanies[j];
              if (activeCompany.parent === event.target.id) {

                if (!flagRerender) {
                  flagRerender = activeCompany.active;
                }
                activeCompany.active = false;
                this.activeCompanies.splice(j, 1);
                this.companiesServices.updateActiveCompanies(this.activeCompanies);


              }
            }
          } else if (event.target.id === companies[i].parent) {
            companies[i].active = false;
            this.activeCompanies.push(companies[i]);
            this.companiesServices.updateActiveCompanies(this.activeCompanies);
          }
        }
        console.log(flagRerender);
        if (this.activeCompanies.length && flagRerender) {
          this.amCharts.render();
        } else if (!this.activeCompanies.length) {
          this.amCharts.destroy();
        }
        let debit = 0;
        let credit = 0;
        let should = 0;
        this.activeCompanies.forEach(company => {
          debit += company.finance.debit;
          credit += company.finance.credit;
          should += company.finance.should;
        });

        this.indicators.forEach(parent => {
          parent.children.forEach(indicator => {
            switch (indicator.id) {
              case'debit':
                if (debit === 0)  {
                  indicator.total = '';
                } else {
                  indicator.total = debit;
                }
                break;
              case'credit':
                if (debit === 0)  {
                  indicator.total = '';
                } else {
                  indicator.total = credit;
                }
                break;
              case'should':
                if (debit === 0)  {
                  indicator.total = '';
                } else {
                  indicator.total = should;
                }
                break;
            }
          });
        });
      },
      error => console.log(error)
    );
  }

  toggleAccordion(menu, link) {
    // menu.forEach(item => {
    //   if (item !== link) { item.active = false; }
    // });
    link.active ? link.active = false : link.active = true;
  }

  toggleActiveCompany(event) {
    this.activeCompanies.forEach(company => {
      if (+event.target.id === company.id && event.target.checked) {
        company.active = true;
      } else if (+event.target.id === company.id && !event.target.checked) {
        company.active = false;
      }
    });
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.activeCompanies.length; i++) {
      if (this.activeCompanies[i].active) {
        this.amCharts.render();
        break;
      }
    }
  }

  onChangeIndicators(event, id) {
    this.companiesServices.updateCompaniesIndicators(id);
    this.amCharts.render();
  }

}
